<?php
/**
 * Created by naf4me@gmail.com.
 * User: Almas Hossain
 * Date: 09-08-2017
 */

require_once("sorting.php");

$array = array(6,1,7,3,9,8,-3,55);
$mysorting = new sorting();

$insertion = $mysorting->insertion($array);
$selection = $mysorting->selection($array);
$bubble = $mysorting->bubble($array);
$merge = $mysorting->merge($array);
$quick = $mysorting->merge($array);


echo('Insertion     :');
echo(implode(",", $insertion));
echo('<br>Selection     :');
echo(implode(",", $selection));
echo('<br>Bubble        :');
echo(implode(",", $bubble));
echo('<br>Merge         :');
echo(implode(",", $merge));
echo('<br>Quick         :');
echo(implode(",", $quick));
