<?php

/**
 * Created by naf4me@gmail.com.
 * User: Almas HOssain
 * Date: 09-08-2017
 * Time: 17:17
 */
class Sorting
{
    /* Algorithm : Insertion sort
    for i = 2:n,
        for (k = i; k > 1 and a[k] < a[k-1]; k--)
            swap a[k,k-1]
    end */
    public function insertion($arr){
        $count = count($arr);
        for ( $i = 1; $i < $count; $i++ ) {
            for($k = $i; $k > 0 && $arr[$k] < $arr[$k-1]; $k--){
                $temp = $arr[$k-1];
                $arr[$k-1] = $arr[$k];
                $arr[$k] = $temp;
            }
        }

        return $arr;
    }

    /* Algorithm : Selection sort
    for i = 1:n,
        k = i
        for j = i+1:n
            if a[j] < a[k], k = j
        swap a[i,k]

    end
    */
    public function selection($arr){
        $count = count($arr);
        for($i = 0; $i < $count; $i++){
            $k = $i;
            for($j = $i + 1; $j < $count; $j++){
                if($arr[$j] < $arr[$k]){
                    $k = $j;
                }
            }

            $temp = $arr[$k];
            $arr[$k] = $arr[$i];
            $arr[$i] = $temp;
        }

        return $arr;
    }

    /* Algorithm : Bubble sort
    for i = 1:n,
        swapped = false
        for j = n:i+1,
            if a[j] < a[j-1],
                swap a[j,j-1]
                swapped = true
        break if not swapped
    end
     */
    public function bubble($arr){
        $count = count($arr);
        for($i = 0; $i < $count; $i++){
            $swapped = false;
            for($j = $count-1; $j >= $i + 1; $j--){
                if($arr[$j] < $arr[$j-1]){
                    $temp = $arr[$j];
                    $arr[$j] = $arr[$j-1];
                    $arr[$j-1] = $temp;
                    $swapped = true;
                }
            }
            if(!$swapped)
                break;
        }

        return $arr;
    }

    /* Algorithm : Merge Sort
    # split in half
    m = n / 2

    # recursive sorts
    sort a[1..m]
    sort a[m+1..n]

    # merge sorted sub-arrays using temp array
    b = copy of a[1..m]
    i = 1, j = m+1, k = 1
    while i <= m and j <= n,
        a[k++] = (a[j] < b[i]) ? a[j++] : b[i++]
        → invariant: a[1..k] in final position
    while i <= m,
        a[k++] = b[i++]
        → invariant: a[1..k] in final position
     */
    public function merge($arr){
        $count = count($arr);
        if($count == 1 )
            return $arr;
        $mid = $count / 2;
        $left = array_slice($arr, 0, $mid);
        $right = array_slice($arr, $mid);
        $left = $this->merge($left);
        $right = $this->merge($right);
        return $this->_merge($left, $right);
    }

    /**
     * @param $left
     * @param $right
     * @return array
     */
    private function _merge($left, $right){
        $res = array();
        while (count($left) > 0 && count($right) > 0){
            if($left[0] > $right[0]){
                $res[] = $right[0];
                $right = array_slice($right , 1);
            }else{
                $res[] = $left[0];
                $left = array_slice($left, 1);
            }
        }
        while (count($left) > 0){
            $res[] = $left[0];
            $left = array_slice($left, 1);
        }
        while (count($right) > 0){
            $res[] = $right[0];
            $right = array_slice($right, 1);
        }

        return $res;
    }

    /* Quick sort algorithm
    # choose pivot_
    swap a[1,rand(1,n)]

    # 2-way partition_
    k = 1
    for i = 2:n, if a[i] < a[1], swap a[++k,i]
    swap a[1,k]

    # recursive sorts_
    sort a[1..k-1]
    sort a[k+1,n]
    */
    public function quick( $arr ) {
        $loe = $gt = array();
        if(count($arr) < 2)
        {
            return $arr;
        }
        $pivot_key = key($arr);
        $pivot = array_shift($arr);
        foreach($arr as $val)
        {
            if($val <= $pivot)
            {
                $loe[] = $val;
            }elseif ($val > $pivot)
            {
                $gt[] = $val;
            }
        }
        return array_merge($this->quick($loe),array($pivot_key=>$pivot),$this->quick($gt));
    }
}
